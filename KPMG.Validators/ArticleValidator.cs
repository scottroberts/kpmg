﻿using KPMG.Contracts;
using KPMG.Model;

namespace KPMG.Validators
{
    public class ArticleValidator : IValidate<Article>
    {
        public bool IsValid(Article article)
        {
            if (string.IsNullOrWhiteSpace(article.Title))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(article.Body))
            {
                return false;
            }

            if (!article.PublishDate.HasValue)
            {
                return false;
            }

            return true;
        }
    }
}