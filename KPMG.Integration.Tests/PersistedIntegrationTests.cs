﻿using System;
using FakeItEasy;
using KPMG.Contracts;
using KPMG.Model;
using KPMG.Repository;
using KPMG.Services;
using KPMG.Validators;
using NUnit.Framework;

namespace KPMG.Integration.Tests
{
    public class PersistedIntegrationTests
    {
        [Test]
        public void ValidArticleIsPersistedToStore()
        {
            var dataStore = new InMemoryDataStore();

            var artcileValidator = new ArticleValidator();
            var articleRepository = new ArticleRepository(dataStore);

            var articleToStore = new Article()
            {
                Title = "a title",
                Body = "a body",
                PublishDate = DateTime.Now
            };

            var publisherService = new PublisherService(artcileValidator, articleRepository);

            publisherService.PublishArticle(articleToStore);

            Assert.That( dataStore.Entities[0], Is.SameAs(articleToStore));
        }


    }
}
