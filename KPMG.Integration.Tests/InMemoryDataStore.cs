﻿using System.Collections.Generic;
using KPMG.Contracts;
using KPMG.Model;

namespace KPMG.Integration.Tests
{
    public class InMemoryDataStore : IDataStore
    {
        public IList<IEntity> Entities { get; }

        public InMemoryDataStore()
        {
            Entities = new List<IEntity>();
        }

        public void Persist(IEntity entity)
        {
            Entities.Add(entity);
        }
    }
}