﻿using KPMG.Model;

namespace KPMG.Contracts
{
    public interface IPublisherService
    {
        void PublishArticle(Article article);
    }
}