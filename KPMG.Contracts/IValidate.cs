﻿namespace KPMG.Contracts
{
    public interface IValidate<in TEntity>
    {
        bool IsValid(TEntity article);
    }
}