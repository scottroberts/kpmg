﻿
namespace KPMG.Contracts
{
    public interface IRepository<in TEntity> where TEntity : class
    {
        void Save(TEntity article);
    }
}