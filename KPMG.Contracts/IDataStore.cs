﻿using KPMG.Model;

namespace KPMG.Contracts
{
    public interface IDataStore
    {
        void Persist(IEntity entity);
    }
}