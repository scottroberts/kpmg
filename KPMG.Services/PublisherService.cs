using KPMG.Contracts;
using KPMG.Model;

namespace KPMG.Services
{
    public class PublisherService : IPublisherService
    {
        private readonly IValidate<Article> _articlValidator;
        private readonly IRepository<Article> _articleRepository;

        public PublisherService(IValidate<Article> articlValidator, IRepository<Article> articleRepository)
        {
            _articlValidator = articlValidator;
            _articleRepository = articleRepository;
        }

        public void PublishArticle(Article article)
        {
            if (_articlValidator.IsValid(article))
            {
                _articleRepository.Save(article);
            }
        }
    }
}