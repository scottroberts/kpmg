using System;

namespace KPMG.Model
{
    public class Article : IEntity
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime? PublishDate { get; set; }
    }
}