using System;
using KPMG.Contracts;
using KPMG.Model;

namespace KPMG.Repository
{
    public class ArticleRepository : IRepository<Article>
    {
        private readonly IDataStore _dataStore;

        public ArticleRepository(IDataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public void Save(Article article)
        {
            _dataStore.Persist(article);            
        }
    }
}