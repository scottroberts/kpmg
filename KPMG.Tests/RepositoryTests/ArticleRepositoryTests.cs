﻿using FakeItEasy;
using KPMG.Contracts;
using KPMG.Model;
using KPMG.Repository;
using NUnit.Framework;

namespace KPMG.Tests.RepositoryTests
{
    public class ArticleRepositoryTests
    {
        [Test]
        public void WhenSaveIsCalledPersistsToDataStore()
        {
            var mockStore = A.Fake<IDataStore>();

            var articleRepostory = new ArticleRepository(mockStore);

            var articleToSave = new Article();

            articleRepostory.Save(articleToSave);

            A.CallTo(() => mockStore.Persist(A<Article>._)).MustHaveHappened();
        }
    }
}
