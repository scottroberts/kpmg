﻿using System;
using KPMG.Model;
using KPMG.Validators;
using NUnit.Framework;

namespace KPMG.Tests.ValidatorTests
{
    public class ArticleValidatorTests
    {
        private Article validArticle;

        [SetUp]
        public void TestSetUp()
        {
            validArticle = new Article()
            {
                Title = "Here is A Title",
                Body = "Here is a body",
                PublishDate = DateTime.Now
            };
        }


        [Test]
        public void WhenArticleHasTitleIsValidReturnsTrue()
        {
            var articleValidator = new ArticleValidator();

            var result = articleValidator.IsValid(validArticle);

            Assert.That( result, Is.True);
        }

        [Test]
        public void WhenArticleDoesNotHaveATitleReturnsFalse()
        {
            var articleToTest = new Article()
            {
                Title = ""
            };

            var articleValidator = new ArticleValidator();

            var result = articleValidator.IsValid(articleToTest);

            Assert.That(result, Is.False);
        }

        [Test]
        public void WhenArticleHasBodyIsValidReturnsTrue()
        {
            var articleValidator = new ArticleValidator();

            var result = articleValidator.IsValid(validArticle);

            Assert.That(result, Is.True);
        }

        [Test]
        public void WhenArticleDoesNotHaveABodyReturnsFalse()
        {
            var articleToTest = new Article()
            {
                Title = "Here is A Title",
                Body = ""
            };

            var articleValidator = new ArticleValidator();

            var result = articleValidator.IsValid(articleToTest);

            Assert.That(result, Is.False);
        }

        [Test]
        public void WhenArticleHasPublishDateIsValidReturnsTrue()
        {
            var articleValidator = new ArticleValidator();

            var result = articleValidator.IsValid(validArticle);

            Assert.That(result, Is.True);
        }

        [Test]
        public void WhenArticleDoesNotHaveAPublishDateReturnsFalse()
        {
            var articleToTest = new Article()
            {
                Title = "Here is A Title",
                Body = "Here is a body",
                PublishDate = null
            };

            var articleValidator = new ArticleValidator();

            var result = articleValidator.IsValid(articleToTest);

            Assert.That(result, Is.False);
        }
    }
}
