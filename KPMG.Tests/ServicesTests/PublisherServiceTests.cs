﻿using FakeItEasy;
using KPMG.Contracts;
using KPMG.Model;
using KPMG.Services;
using NUnit.Framework;

namespace KPMG.Tests.Services
{
    public class PublisherServiceTests
    {
        private IValidate<Article> mockValidator;
        private IRepository<Article> mockRepository;

        [SetUp]
        public void TestSetup()
        {
            mockValidator = A.Fake<IValidate<Article>>();
            mockRepository = A.Fake<IRepository<Article>>();

            A.CallTo(() => mockValidator.IsValid(A<Article>._)).Returns(true);
        }

        [Test]
        public void WhenGivenNewArticlePublishesIT()
        {
            var article = new Article();

            var publisher = new PublisherService(mockValidator, mockRepository);

            publisher.PublishArticle(article);

            A.CallTo(() => mockRepository.Save(A<Article>._)).MustHaveHappened();
        }

        [Test]
        public void WhenGivenNewArticleItIsValidated()
        {
            var article = new Article();

            var publisher = new PublisherService(mockValidator, mockRepository);

            publisher.PublishArticle(article);

            A.CallTo(() => mockValidator.IsValid(A<Article>._)).MustHaveHappened();
        }

        [Test]
        public void WhenNewArticleFailsValidationDoesNotPublish()
        {
            var article = new Article();

            A.CallTo(() => mockValidator.IsValid(A<Article>._)).Returns(false);

            var publisher = new PublisherService(mockValidator, mockRepository);

            publisher.PublishArticle(article);

            A.CallTo(() => mockRepository.Save(A<Article>._)).MustNotHaveHappened();
        }

        [Test]
        public void WhenNewArticlePassesValidationItIsPublished()
        {
            var article = new Article();

            var publisher = new PublisherService(mockValidator, mockRepository);

            publisher.PublishArticle(article);

            A.CallTo(() => mockRepository.Save(A<Article>._)).MustHaveHappened();
        }
    }
}
